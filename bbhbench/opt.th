!CRL_VERSION = 1.0

!DEFINE ROOT = BinaryBlackHoles
!DEFINE ARR  = $ROOT/arrangements
!DEFINE COMPONENTLIST_TARGET = $ROOT/thornlists/

!DEFINE ET_RELEASE = ET_2016_05


# This thorn list
!TARGET   = $ROOT
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/manifest.git
!REPO_PATH= $1
!NAME     = manifest
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = ./manifest

# Cactus Flesh
!TARGET   = $ROOT
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactus.git
!NAME     = flesh
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = .clang-format CONTRIBUTORS COPYRIGHT doc lib Makefile src

# Simulation Factory
!TARGET   = $ROOT
!TYPE     = git
!URL      = https://bitbucket.org/simfactory/simfactory2.git
!REPO_PATH=$1
!NAME     = simfactory2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = ./simfactory

# Simulation Factory 3
!TARGET   = $ROOT
!TYPE     = git
!URL      = git@bitbucket.org:ianhinder/simfactory3.git
!REPO_PATH=$1
!NAME     = simfactory3
!CHECKOUT = ./simfactory3

# Example parameter files
!TARGET   = $ROOT
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/einsteinexamples.git
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = par

# Various Cactus utilities
!TARGET   = $ROOT
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/utilities.git
!REPO_PATH= $1
!NAME     = utils
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = ./utils

# The GetComponents script
!TARGET   = $ROOT/bin
!TYPE     = git
!URL      = https://github.com/gridaphobe/CRL.git
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = GetComponents



# CactusBase thorns
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactusbase.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
CactusBase/Boundary
CactusBase/CartGrid3D
CactusBase/CoordBase
CactusBase/Fortran
CactusBase/InitBase
CactusBase/IOUtil
CactusBase/SymBase
CactusBase/Time

# CactusDoc thorns
!TARGET   = $ARR/CactusDoc
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/coredoc.git
!NAME     = CoreDoc
!REPO_PATH= $1
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = ./CoreDoc

# CactusElliptic thorns
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactuselliptic.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = CactusElliptic/EllPETSc CactusElliptic/TATPETSc

# CactusNumerical thorns
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactusnumerical.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
CactusNumerical/LocalInterp
CactusNumerical/MoL
CactusNumerical/Slab
CactusNumerical/SpaceMask
CactusNumerical/SphericalSurface
CactusNumerical/SummationByParts
CactusNumerical/TensorTypes

# CactusTest thorns
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactustest.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = CactusTest/TestAllTypes

# CactusUtils thorns
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/cactusutils.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
CactusUtils/Formaline
CactusUtils/NaNChecker
CactusUtils/NoMPI
CactusUtils/SystemStatistics
CactusUtils/SystemTopology
CactusUtils/TerminationTrigger
CactusUtils/Vectors
CactusUtils/Trigger

# EinsteinAnalysis
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/einsteinanalysis.git
!REPO_PATH= $2
!REPO_BRANCH = rhaas/reduce_io
#REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
EinsteinAnalysis/AHFinderDirect
EinsteinAnalysis/Multipole
EinsteinAnalysis/PunctureTracker
EinsteinAnalysis/QuasiLocalMeasures
EinsteinAnalysis/WeylScal4

# EinsteinBase
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/einsteinbase.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
EinsteinBase/ADMBase
EinsteinBase/ADMCoupling        # deprecated
EinsteinBase/ADMMacros          # deprecated
EinsteinBase/CoordGauge
EinsteinBase/StaticConformal
EinsteinBase/TmunuBase

# EinsteinEvolve
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/einsteinevolve.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
EinsteinEvolve/NewRad

# EinsteinInitialData
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/einsteininitialdata.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
EinsteinInitialData/TwoPunctures

# EinsteinUtils
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/einsteinutils.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
EinsteinUtils/TGRtensor



# Carpet, the AMR driver
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/eschnett/carpet.git
!REPO_PATH= $2
!REPO_BRANCH = rhaas/openmp-tasks
!CHECKOUT = Carpet/doc
Carpet/Carpet
Carpet/CarpetIOASCII
Carpet/CarpetIOBasic
Carpet/CarpetIOHDF5
Carpet/CarpetIOScalar
Carpet/CarpetInterp
Carpet/CarpetInterp2
Carpet/CarpetLib
Carpet/CarpetMask
Carpet/CarpetReduce
Carpet/CarpetRegrid2
Carpet/CarpetTracker
Carpet/CycleClock
Carpet/LoopControl
Carpet/Timers



!TARGET   = $ARR
!TYPE     = git
!URL      = https://github.com/barrywardell/EinsteinExact.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = EinsteinExact/doc EinsteinExact/m EinsteinExact/tests



# Additional Cactus thorns
!TARGET   = $ARR
!TYPE     = svn
!URL      = https://svn.cactuscode.org/projects/$1/$2/branches/$ET_RELEASE
!CHECKOUT = ExternalLibraries/OpenBLAS ExternalLibraries/pciutils ExternalLibraries/PETSc
ExternalLibraries/BLAS
ExternalLibraries/GSL
ExternalLibraries/HDF5
ExternalLibraries/hwloc
ExternalLibraries/LAPACK
ExternalLibraries/MPI
#DISABLED ExternalLibraries/OpenBLAS
#DISABLED ExternalLibraries/pciutils
ExternalLibraries/zlib



# From Kranc (required e.g. by McLachlan)
!TARGET   = $ARR
!TYPE     = git
!URL      = https://github.com/ianhinder/Kranc.git
!REPO_PATH= Auxiliary/Cactus
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
KrancNumericalTools/GenericFD



# McLachlan, the spacetime code
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/einsteintoolkit/mclachlan.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT = McLachlan/doc McLachlan/m McLachlan/par
McLachlan/ML_BSSN
McLachlan/ML_BSSN_Helper
McLachlan/ML_CCZ4
McLachlan/ML_CCZ4_Helper
McLachlan/ML_ADMConstraints

# Numerical
!TARGET   = $ARR
!TYPE     = git
!URL      = https://bitbucket.org/cactuscode/numerical.git
!REPO_PATH= $2
!REPO_BRANCH = $ET_RELEASE
!CHECKOUT =
Numerical/AEILocalInterp



# Private thorns
!TARGET   = $ARR
!TYPE     = ignore
!CHECKOUT =

# BinaryBlackHoles
!TARGET   = $ROOT/extras
!TYPE     = git
!URL      = git@bitbucket.org:ianhinder/binaryblackholes.git
!REPO_PATH=$1
!REPO_BRANCH = master
!CHECKOUT = BinaryBlackHoles


# Llama
!TARGET   = $ARR
!TYPE     = git
!AUTH_URL = https://bitbucket.org/llamacode/llama.git
!URL      = https://bitbucket.org/llamacode/llama.git
!REPO_PATH= $2
!REPO_BRANCH = master
!CHECKOUT =
#Llama/ADMDerivatives
#Llama/WaveExtractL
Llama/ADMDerivatives
Llama/WaveExtractCPM
Llama/Coordinates
Llama/CoordinatesSymmetry
Llama/Interpolate2
Llama/WorldTube
Llama/GlobalDerivative
Llama/SphericalSlice

!TARGET   = $ROOT/extras
!TYPE     = git
!URL      = https://bitbucket.org/simulationtools/SimulationTools.git
!REPO_PATH = $1
!CHECKOUT = ./SimulationTools

!TARGET   = $ROOT/extras
!TYPE     = http
!URL      = http://bitbucket.org/simulationtools/h5mma/downloads
!CHECKOUT = h5mma-1.2.1.tar.gz

# The getsim script
!TARGET   = $ROOT/bin
!TYPE     = git
!URL      = https://bitbucket.org/ianhinder/getsim.git
!REPO_BRANCH = master
!CHECKOUT = getsim

RunView/RunView
RunView/RunViewConfig
# ExternalLibraries/PAPI
